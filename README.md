# TypeGrid

![DEMO](assets/demo.png)

## Description

This application utilizes a type grid system to help users determine their Jungian personality type.
This information can help users gain insights into their own behaviors, motivations, and tendencies,
and can be a helpful tool for personal growth and development.

One's personality type can be identified by analyzing the traits that they display. Although every 
individual is capable of using any cognitive function and exhibiting any trait, their preference is
influenced by their personality type. This application enables counting the occurences of each
personality trait and distributes the probabilities among all types, which are presented in a dynamic
type grid.

## Build Process

Make sure you have the latest version of Rust installed on your system and then run

``` sh
cargo build --release
```


## License

TypeGrid is licensed under [GPL 3.0](COPYING).

## Donate

Help us keep the peace and throw some coins our way! With your donation, we can help people speak 
the same language and prevent any karate chops to the face. Plus, you'll earn good karma points!

- Bitcoin: bc1qyv7wmw4yq9m5gwnfjld0tl3xcgj0jr4mu3pxm5
- Monero: 85PL4mzXWt4chnWjdmnsRP19DbndU8ohqPWd9DD2j4MVFCdUfEHh3CvAvoBDqFBRBVR6uCxcWNWDLPsZTwuUZzPK8bhUYhV

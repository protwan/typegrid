/*
 * Copyright (C) 2022  protwan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use druid::{Color, Data};

#[derive(Debug, Clone, Copy, Data)]
pub struct Theme {
    pub background_color: Color,
    pub foreground_color: Color,
    pub primary_color: Color,
}

impl Theme {
    pub fn light() -> Self {
        Self {
            background_color: Color::WHITE,
            foreground_color: Color::BLACK,
            primary_color: Color::GRAY,
        }
    }

    pub fn dark() -> Self {
        Self {
            background_color: Color::rgb8(28, 32, 35),
            foreground_color: Color::rgb8(199, 204, 209),
            primary_color: Color::GRAY,
        }
    }

    pub fn with_primary(mut self, hue: f64) -> Self {
        self.primary_color = Color::hlc(hue, 65.0, 40.0);
        self
    }
}

pub struct Hue;
impl Hue {
    pub const RED: f64 = 15.0;
    pub const ORANGE: f64 = 60.0;
    pub const YELLOW: f64 = 90.0;
    pub const GREEN: f64 = 120.0;
    pub const BLUE: f64 = 265.0;
    pub const PURPLE: f64 = 300.0;
}

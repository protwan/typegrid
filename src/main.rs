/*
 * Copyright (C) 2022  protwan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#![windows_subsystem = "windows"]

mod themes;

use std::time::Duration;

use druid::{
    theme,
    widget::{Align, Flex, Label, LabelText, Painter},
    AppDelegate, AppLauncher, BoxConstraints, Command, Data, DelegateCtx, Env, Event, EventCtx,
    FontDescriptor, FontWeight, Handled, LayoutCtx, Lens, LifeCycle, LifeCycleCtx, LocalizedString,
    Menu, MenuItem, PaintCtx, RenderContext, Selector, Size, Target, UpdateCtx, Widget, WidgetExt,
    WindowDesc, WindowId,
};
use themes::{Hue, Theme};

#[derive(Debug, Clone, Data, Lens)]
struct Model {
    theme: Theme,

    //dist: [f64; 16],
    dist: [[f64; 5]; 5],

    // Interaction style
    initiating: u8,
    responding: u8,

    direct: u8,
    informative: u8,

    progression: u8,
    outcome: u8,

    // Temperament
    concrete: u8,
    abstracting: u8,

    systematic: u8,
    interest: u8,

    affiliative: u8,
    pragmatic: u8,

    // Quadra
    tefi: u8,
    tife: u8,

    nesi: u8,
    nise: u8,
}

impl Model {
    fn update(&mut self) {
        fn pr_axis(c1: u8, c2: u8) -> (f64, f64) {
            let c1 = match c1 {
                0 => f64::EPSILON,
                n => n as f64,
            };

            let c2 = match c2 {
                0 => f64::EPSILON,
                n => n as f64,
            };

            (c1 / (c1 + c2), c2 / (c1 + c2))
        }

        let (pr_concrete, pr_abstract) = pr_axis(self.concrete, self.abstracting);
        let (pr_systematic, pr_interest) = pr_axis(self.systematic, self.interest);
        let (pr_affiliative, pr_pragmatic) = pr_axis(self.affiliative, self.pragmatic);

        let pr_guardian = pr_concrete * pr_systematic * pr_affiliative;
        let pr_artisan = pr_concrete * pr_interest * pr_pragmatic;
        let pr_intellectual = pr_abstract * pr_systematic * pr_pragmatic;
        let pr_idealist = pr_abstract * pr_interest * pr_affiliative;

        let (pr_initiating, pr_responding) = pr_axis(self.initiating, self.responding);
        let (pr_direct, pr_informative) = pr_axis(self.direct, self.informative);
        let (pr_progression, pr_outcome) = pr_axis(self.progression, self.outcome);

        let pr_structure = pr_initiating * pr_direct * pr_outcome;
        let pr_starter = pr_initiating * pr_informative * pr_progression;
        let pr_finisher = pr_responding * pr_direct * pr_progression;
        let pr_background = pr_responding * pr_informative * pr_outcome;

        let (pr_tefi, pr_tife) = pr_axis(self.tefi, self.tife);
        let (pr_nesi, pr_nise) = pr_axis(self.nesi, self.nise);

        let pr_templar = pr_tife * pr_nise;
        let pr_crusader = pr_tife * pr_nesi;
        let pr_wayfarer = pr_tefi * pr_nise;
        let pr_philosopher = pr_tefi * pr_nesi;

        let outcomes = [
            pr_structure * pr_guardian * pr_philosopher,
            pr_structure * pr_artisan * pr_templar,
            pr_structure * pr_intellectual * pr_wayfarer,
            pr_structure * pr_idealist * pr_templar,
            pr_starter * pr_guardian * pr_crusader,
            pr_starter * pr_artisan * pr_wayfarer,
            pr_starter * pr_intellectual * pr_crusader,
            pr_starter * pr_idealist * pr_philosopher,
            pr_finisher * pr_guardian * pr_philosopher,
            pr_finisher * pr_artisan * pr_templar,
            pr_finisher * pr_intellectual * pr_wayfarer,
            pr_finisher * pr_idealist * pr_templar,
            pr_background * pr_guardian * pr_crusader,
            pr_background * pr_artisan * pr_wayfarer,
            pr_background * pr_intellectual * pr_crusader,
            pr_background * pr_idealist * pr_philosopher,
        ];

        let pr_sample_space: f64 = outcomes.into_iter().sum();
        let pr_dist = outcomes.map(|outcome| outcome / pr_sample_space);

        // Reset matrix to zeros. This is necessary because row and column headers add to the
        // existing value in the matrix
        self.dist = [[0.0; 5]; 5];
        for i in 0..4 {
            for j in 0..4 {
                // First cell should contain total probability of all types. It should always be 1.0,
                // but in case there is a bug, at least this should work properly :P
                self.dist[0][0] += pr_dist[i * 4 + j];

                // First cell in every row contains total probability of that row
                self.dist[i + 1][0] += pr_dist[i * 4 + j];

                // First cell in every column contains total probability of that column
                self.dist[0][j + 1] += pr_dist[i * 4 + j];

                // The rest of the cells contain probabilities for every type
                self.dist[i + 1][j + 1] = pr_dist[i * 4 + j];
            }
        }
    }
}

impl Default for Model {
    fn default() -> Self {
        Self {
            theme: Theme::light().with_primary(Hue::BLUE),
            dist: [
                [1.0, 0.25, 0.25, 0.25, 0.25],
                [0.25, 0.0625, 0.0625, 0.0625, 0.0625],
                [0.25, 0.0625, 0.0625, 0.0625, 0.0625],
                [0.25, 0.0625, 0.0625, 0.0625, 0.0625],
                [0.25, 0.0625, 0.0625, 0.0625, 0.0625],
            ],

            initiating: 0,
            responding: 0,
            direct: 0,
            informative: 0,
            progression: 0,
            outcome: 0,
            concrete: 0,
            abstracting: 0,
            systematic: 0,
            interest: 0,
            affiliative: 0,
            pragmatic: 0,
            tefi: 0,
            tife: 0,
            nesi: 0,
            nise: 0,
        }
    }
}

struct TypeGridCell {
    current: f64,
    start: f64,
    end: f64,
    duration: Duration,
    elapsed: Duration,
    label: Align<f64>,
}

impl TypeGridCell {
    fn new<L>(label: L) -> Self
    where
        L: Into<LabelText<f64>>,
    {
        Self {
            label: Label::new(label).center(),
            current: Default::default(),
            start: Default::default(),
            end: Default::default(),
            duration: Duration::from_millis(500),
            elapsed: Duration::from_millis(0),
        }
    }
}

impl Widget<f64> for TypeGridCell {
    fn event(&mut self, ctx: &mut EventCtx, event: &Event, data: &mut f64, env: &Env) {
        match event {
            Event::AnimFrame(interval) => {
                ctx.request_paint();
                self.elapsed += Duration::from_nanos(*interval);
                let perc = self.elapsed.as_secs_f64() / self.duration.as_secs_f64();
                if perc < 1.0 {
                    self.current = self.start * (1.0 - perc) + self.end * perc;
                    ctx.request_anim_frame();
                } else {
                    self.current = self.end;
                }
            }
            Event::WindowConnected => {
                ctx.request_update();
            }
            _ => {}
        }
        self.label.event(ctx, event, data, env);
    }

    fn update(&mut self, ctx: &mut UpdateCtx, old_data: &f64, data: &f64, env: &Env) {
        self.start = *old_data;
        self.end = *data;
        self.current = *old_data;
        self.elapsed = Duration::from_millis(0);
        ctx.request_anim_frame();
        self.label.update(ctx, old_data, data, env);
    }

    fn paint(&mut self, ctx: &mut PaintCtx, data: &f64, env: &Env) {
        let color = &env.get(theme::PRIMARY_DARK).with_alpha(self.current);
        let bounds = ctx.size().to_rect();
        ctx.fill(bounds, color);
        self.label.paint(ctx, data, env);
    }

    fn lifecycle(&mut self, ctx: &mut LifeCycleCtx, event: &LifeCycle, data: &f64, env: &Env) {
        self.label.lifecycle(ctx, event, data, env);
    }

    fn layout(&mut self, ctx: &mut LayoutCtx, bc: &BoxConstraints, data: &f64, env: &Env) -> Size {
        self.label.layout(ctx, bc, data, env)
    }
}

fn typegrid_view() -> impl Widget<Model> {
    [
        ["", "Guardian", "Artisan", "Intellectual", "Idealist"],
        ["Structure", "ESTJ", "ESTP", "ENTJ", "ENFJ"],
        ["Starter", "ESFJ", "ESFP", "ENTP", "ENFP"],
        ["Finisher", "ISTJ", "ISTP", "INTJ", "INFJ"],
        ["Background", "ISFJ", "ISFP", "INTP", "INFP"],
    ]
    .into_iter()
    .enumerate()
    .map(|(i, row)| {
        row.into_iter()
            .enumerate()
            .fold(Flex::row(), |mut row, (j, curr)| {
                // Prepend spacer if it's not the first item in row
                if j != 0 {
                    row.add_default_spacer();
                }

                row.with_flex_child(
                    TypeGridCell::new(curr).lens(druid::lens!(Model, dist[i][j])),
                    1.0,
                )
            })
    })
    .enumerate()
    .fold(Flex::column(), |mut col, (i, row)| {
        // Prepend spacer if it's not the first row
        if i != 0 {
            col.add_default_spacer();
        }
        col.with_flex_child(row, 1.0)
    })
}

fn counter_view<S, L>(label: S, lens: L) -> impl Widget<Model>
where
    S: Into<LabelText<Model>>,
    L: Lens<Model, u8> + Clone + Copy + 'static,
{
    Flex::column()
        .with_child(Label::new(label).center())
        .with_default_spacer()
        .with_flex_child(
            Flex::row()
                .with_flex_child(
                    Label::new("－")
                        .with_font(
                            FontDescriptor::default()
                                .with_weight(FontWeight::BOLD)
                                .with_size(18.0),
                        )
                        .center()
                        .background(Painter::new(|ctx, _, env| {
                            let bounds = ctx.size().to_rect();
                            ctx.fill(bounds, &env.get(theme::BUTTON_LIGHT));
                        }))
                        .on_click(move |_ctx, model: &mut Model, _env: &_| {
                            lens.with_mut(model, |data| {
                                *data = data.saturating_sub(1);
                            });
                            model.update();
                        })
                        .expand(),
                    1.0,
                )
                .with_flex_child(
                    Label::new(|data: &u8, _: &_| data.to_string())
                        .lens(lens)
                        .center(),
                    1.5,
                )
                .with_flex_child(
                    Label::new("＋")
                        .with_font(
                            FontDescriptor::default()
                                .with_weight(FontWeight::BOLD)
                                .with_size(18.0),
                        )
                        .center()
                        .background(Painter::new(|ctx, _, env| {
                            let bounds = ctx.size().to_rect();
                            ctx.fill(bounds, &env.get(theme::BUTTON_LIGHT));
                        }))
                        .on_click(move |_ctx, model: &mut Model, _env: &_| {
                            lens.with_mut(model, |data| {
                                *data = data.saturating_add(1);
                            });
                            model.update();
                        })
                        .expand(),
                    1.0,
                )
                .border(theme::BORDER_LIGHT, 1.0),
            1.0,
        )
}

fn interaction_style_view() -> impl Widget<Model> {
    Flex::column()
        .with_flex_child(
            Flex::row()
                .with_flex_child(counter_view("Initiating", Model::initiating).center(), 5.0)
                .with_flex_spacer(1.0)
                .with_flex_child(counter_view("Responding", Model::responding).center(), 5.0),
            5.0,
        )
        .with_flex_spacer(1.0)
        .with_flex_child(
            Flex::row()
                .with_flex_child(counter_view("Direct", Model::direct).center(), 5.0)
                .with_flex_spacer(1.0)
                .with_flex_child(
                    counter_view("Informative", Model::informative).center(),
                    5.0,
                ),
            5.0,
        )
        .with_flex_spacer(1.0)
        .with_flex_child(
            Flex::row()
                .with_flex_child(
                    counter_view("Progression", Model::progression).center(),
                    5.0,
                )
                .with_flex_spacer(1.0)
                .with_flex_child(counter_view("Outcome", Model::outcome).center(), 5.0),
            5.0,
        )
        .padding(10.0)
}

fn temperament_view() -> impl Widget<Model> {
    Flex::row()
        .with_flex_child(
            Flex::column()
                .with_flex_child(counter_view("Concrete", Model::concrete).center(), 5.0)
                .with_flex_spacer(1.0)
                .with_flex_child(counter_view("Abstract", Model::abstracting).center(), 5.0)
                .with_flex_spacer(1.0),
            5.0,
        )
        .with_flex_spacer(1.0)
        .with_flex_child(
            Flex::column()
                .with_flex_child(counter_view("Systematic", Model::systematic).center(), 5.0)
                .with_flex_spacer(1.0)
                .with_flex_child(counter_view("Interest", Model::interest).center(), 5.0)
                .with_flex_spacer(1.0),
            5.0,
        )
        .with_flex_spacer(1.0)
        .with_flex_child(
            Flex::column()
                .with_flex_child(
                    counter_view("Affiliative", Model::affiliative).center(),
                    5.0,
                )
                .with_flex_spacer(1.0)
                .with_flex_child(counter_view("Pragmatic", Model::pragmatic).center(), 5.0)
                .with_flex_spacer(1.0),
            5.0,
        )
        .padding(10.0)
}

fn quadra_view() -> impl Widget<Model> {
    Flex::row()
        .with_flex_child(
            Flex::column()
                .with_flex_child(counter_view("Te / Fi", Model::tefi).center(), 5.0)
                .with_flex_spacer(1.0)
                .with_flex_child(counter_view("Ti / Fe", Model::tife).center(), 5.0)
                .with_flex_spacer(1.0),
            5.0,
        )
        .with_flex_spacer(1.0)
        .with_flex_child(
            Flex::column()
                .with_flex_child(counter_view("Ne / Si", Model::nesi).center(), 5.0)
                .with_flex_spacer(1.0)
                .with_flex_child(counter_view("Ni / Se", Model::nise).center(), 5.0)
                .with_flex_spacer(1.0),
            5.0,
        )
        .padding(10.0)
}

fn view() -> impl Widget<Model> {
    Flex::column()
        .with_flex_child(
            Flex::row()
                .with_flex_child(typegrid_view().center().padding(10.0), 3.0)
                .with_flex_child(
                    interaction_style_view()
                        .border(theme::BORDER_LIGHT, 1.0)
                        .center()
                        .padding(10.0),
                    2.0,
                ),
            3.0,
        )
        .with_flex_child(
            Flex::row()
                .with_flex_child(
                    temperament_view()
                        .border(theme::BORDER_LIGHT, 1.0)
                        .center()
                        .padding(10.0),
                    3.0,
                )
                .with_flex_child(
                    quadra_view()
                        .border(theme::BORDER_LIGHT, 1.0)
                        .center()
                        .padding(10.0),
                    2.0,
                ),
            2.0,
        )
        .env_scope(|env, model| {
            env.set(theme::WINDOW_BACKGROUND_COLOR, model.theme.background_color);
            env.set(theme::BACKGROUND_DARK, model.theme.background_color);
            env.set(theme::BACKGROUND_LIGHT, model.theme.background_color);
            env.set(theme::FOREGROUND_DARK, model.theme.foreground_color);
            env.set(theme::FOREGROUND_LIGHT, model.theme.foreground_color);
            env.set(theme::TEXT_COLOR, model.theme.foreground_color);
            env.set(theme::PRIMARY_DARK, model.theme.primary_color);
            env.set(
                theme::PRIMARY_LIGHT,
                model.theme.primary_color.with_alpha(0.125),
            );
            env.set(
                theme::BUTTON_DARK,
                model.theme.primary_color.with_alpha(0.125),
            );
            env.set(
                theme::BUTTON_LIGHT,
                model.theme.primary_color.with_alpha(0.125),
            );
            env.set(theme::BUTTON_BORDER_RADIUS, 0.0);
            env.set(theme::BUTTON_BORDER_WIDTH, 0.0);
            env.set(theme::WIDGET_PADDING_HORIZONTAL, 5.0);
            env.set(theme::WIDGET_PADDING_VERTICAL, 5.0);
            env.set(theme::BORDER_DARK, model.theme.primary_color);
            env.set(
                theme::BORDER_LIGHT,
                model.theme.primary_color.with_alpha(0.125),
            );
        })
        .background(Painter::new(|ctx, model: &Model, _env| {
            // For some reason it doesn't update windows background from theme::WINDOW_BACKGROUND_COLOR
            let bounds = ctx.size().to_rect();
            ctx.fill(bounds, &model.theme.background_color);
        }))
}

fn menu_view(_window_id: Option<WindowId>, _model: &Model, _env: &Env) -> Menu<Model> {
    Menu::empty()
        .entry(
            Menu::new(LocalizedString::new("common-menu-file-menu"))
                .entry(MenuItem::new("Quit").command(druid::commands::QUIT_APP)),
        )
        .entry(
            Menu::new(LocalizedString::new("common-menu-edit-menu"))
                .entry(MenuItem::new("Reset").command(MenuCommand::reset())),
        )
        .entry(
            Menu::new(LocalizedString::new("Theme"))
                .entry(
                    Menu::new(LocalizedString::new("Light"))
                        .entry(
                            MenuItem::new(LocalizedString::new("Gray"))
                                .command(MenuCommand::set_theme(Theme::light())),
                        )
                        .entry(MenuItem::new(LocalizedString::new("Red")).command(
                            MenuCommand::set_theme(Theme::light().with_primary(Hue::RED)),
                        ))
                        .entry(MenuItem::new(LocalizedString::new("Orange")).command(
                            MenuCommand::set_theme(Theme::light().with_primary(Hue::ORANGE)),
                        ))
                        .entry(MenuItem::new(LocalizedString::new("Yellow")).command(
                            MenuCommand::set_theme(Theme::light().with_primary(Hue::YELLOW)),
                        ))
                        .entry(MenuItem::new(LocalizedString::new("Green")).command(
                            MenuCommand::set_theme(Theme::light().with_primary(Hue::GREEN)),
                        ))
                        .entry(MenuItem::new(LocalizedString::new("Blue")).command(
                            MenuCommand::set_theme(Theme::light().with_primary(Hue::BLUE)),
                        ))
                        .entry(MenuItem::new(LocalizedString::new("Purple")).command(
                            MenuCommand::set_theme(Theme::light().with_primary(Hue::PURPLE)),
                        )),
                )
                .entry(
                    Menu::new(LocalizedString::new("Dark"))
                        .entry(
                            MenuItem::new(LocalizedString::new("Gray"))
                                .command(MenuCommand::set_theme(Theme::dark())),
                        )
                        .entry(
                            MenuItem::new(LocalizedString::new("Red")).command(
                                MenuCommand::set_theme(Theme::dark().with_primary(Hue::RED)),
                            ),
                        )
                        .entry(MenuItem::new(LocalizedString::new("Orange")).command(
                            MenuCommand::set_theme(Theme::dark().with_primary(Hue::ORANGE)),
                        ))
                        .entry(MenuItem::new(LocalizedString::new("Yellow")).command(
                            MenuCommand::set_theme(Theme::dark().with_primary(Hue::YELLOW)),
                        ))
                        .entry(MenuItem::new(LocalizedString::new("Green")).command(
                            MenuCommand::set_theme(Theme::dark().with_primary(Hue::GREEN)),
                        ))
                        .entry(MenuItem::new(LocalizedString::new("Blue")).command(
                            MenuCommand::set_theme(Theme::dark().with_primary(Hue::BLUE)),
                        ))
                        .entry(MenuItem::new(LocalizedString::new("Purple")).command(
                            MenuCommand::set_theme(Theme::dark().with_primary(Hue::PURPLE)),
                        )),
                ),
        )
}

struct MenuCommand;
impl MenuCommand {
    const RESET: Selector<()> = Selector::new("reset");
    const SET_THEME: Selector<Theme> = Selector::new("set_theme");

    fn reset() -> Command {
        Command::new(Self::RESET, (), Target::Global)
    }

    fn set_theme(theme: Theme) -> Command {
        Command::new(Self::SET_THEME, theme, Target::Global)
    }
}

struct Delegate;
impl AppDelegate<Model> for Delegate {
    fn command(
        &mut self,
        _ctx: &mut DelegateCtx,
        _target: Target,
        cmd: &Command,
        data: &mut Model,
        _env: &Env,
    ) -> Handled {
        if let Some(theme) = cmd.get(MenuCommand::SET_THEME) {
            data.theme = theme.to_owned();
            return Handled::Yes;
        }
        if let Some(()) = cmd.get(MenuCommand::RESET) {
            *data = Model {
                theme: data.theme,
                ..Default::default()
            };
            return Handled::Yes;
        }

        Handled::No
    }
}

fn main() {
    let window = WindowDesc::new(view())
        .title(env!("CARGO_PKG_NAME"))
        .with_min_size((800.0, 500.0))
        .window_size((800.0, 500.0))
        .resizable(true)
        .menu(menu_view);
    AppLauncher::with_window(window)
        .delegate(Delegate)
        .launch(Model::default())
        .unwrap();
}
